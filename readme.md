
# Django Recruitment

Django recruitment is app based on interview task

## Installation

use docker to install:

```bash
django-compose build
django-compose up
```

## Usage

- test deployed at http://194.195.242.78
- api overview is at **/api/**
- swagger overview is at **/api/swagger/**

### API Overview
- candidates - all methods allowed, no authentication needed. Endpoint + Detail info at **/api/candidates/**
- jobs - all methods allowed, no authentication needed. Endpoint + Detail info at **/api/jobs/**
- applications- all methods allowed, no authentication needed. Endpoint + Detail info at **/api/applications/**

## How to run tests
- open docker container with "app_web"
- run in bash command: 
```
python manage.py test 
```


