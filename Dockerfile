FROM python:3.8.7

COPY app /var/www/app
WORKDIR /var/www/app

RUN pip3 install --no-cache-dir -r requirements

RUN chmod +x startup.sh

CMD ["./startup.sh"]
