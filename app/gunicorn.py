import os

module = "recruitment"
name = module
workers = 4
bind = ["[::]:8080"]  # IPv6 alias to IPv4 0.0.0.0
user = "root"
loglevel = "info"
errorlog = "-"
timeout = 900
reload = True if "true" == os.getenv("GUNICORN_RELOAD") else False
limitrequestline = 0

print(f"Starting {name} as {user}.")
