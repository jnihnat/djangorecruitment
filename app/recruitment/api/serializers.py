from rest_framework import serializers

from .models import Application, Candidate, Job


class CandidateSerializer(serializers.ModelSerializer):
    """
    Serializer for Candidate model
    """

    class Meta:
        model = Candidate
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "expected_salary",
            "salary_currency",
            "skills",
        )


class JobSerializer(serializers.ModelSerializer):
    """
    Serializer for Job model
    """

    class Meta:
        model = Job
        fields = (
            "id",
            "job_title",
            "job_description",
            "salary",
            "salary_currency",
        )


class ApplicationSerializer(serializers.ModelSerializer):
    """
    Serializer for Application model
    """

    class Meta:
        model = Application
        fields = (
            "id",
            "applicant",
            "job",
            "status",
        )


class ApplicationFullInfoSerializer(serializers.ModelSerializer):
    """
    Serializer for Application model with full data from foreign key
    """

    applicant = CandidateSerializer()
    job = JobSerializer()

    class Meta:
        model = Application
        fields = (
            "id",
            "applicant",
            "job",
            "status",
        )
