from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from .models import Candidate, Job, Application
from rest_framework import viewsets
from rest_framework.response import Response
from .serializers import (
    CandidateSerializer,
    JobSerializer,
    ApplicationSerializer,
    ApplicationFullInfoSerializer,
)
from django.views.generic.base import RedirectView


class CandidateViewSet(viewsets.ModelViewSet):
    """
    API endpoint for Candidates
    """

    # http_method_names = ["get", "post", "put"]
    # permission_classes = [IsAuthenticated]
    queryset = Candidate.objects.all().order_by("id")
    serializer_class = CandidateSerializer


class JobViewSet(viewsets.ModelViewSet):
    """
    API endpoint for Jobs
    """

    # http_method_names = ["get", "post", "put"]
    # permission_classes = [IsAuthenticated]
    queryset = Job.objects.all().order_by("id")
    serializer_class = JobSerializer


class ApplicantionViewSet(viewsets.ModelViewSet):
    """
    API endpoint for Applications
    """

    # http_method_names = ["get", "post", "put"]
    # permission_classes = [IsAuthenticated]

    queryset = Application.objects.all().order_by("id")
    serializer_class = ApplicationSerializer

    def list(self, request):
        queryset = Application.objects.all().order_by("id")
        serializer = ApplicationFullInfoSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk):
        queryset = Application.objects.all()
        serializer = ApplicationFullInfoSerializer(queryset, many=True)
        return Response(serializer.data)


class ApiRedirectView(RedirectView):
    is_permanent = True
    url = "/api/"
