from django.contrib import admin

from .models import Application, Job, Candidate

admin.site.register(Application)
admin.site.register(Candidate)
admin.site.register(Job)
