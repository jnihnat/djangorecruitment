from django.db import models


class Currency(models.TextChoices):
    CZK = "CZK", "Czech crown"
    EUR = "EUR", "Euro"
    USD = "USD", "US Dolar"


class Status(models.TextChoices):
    AP = "AP", "Applied"
    IP = "IP", "In progress"
    HI = "HI", "Hired"
    NH = "NH", "Not hired"


class Candidate(models.Model):
    """
    model for basic candidate info
    """

    first_name = models.CharField("First Name", max_length=50)
    last_name = models.CharField("Last Name", max_length=50)
    email = models.EmailField("Email", unique=True)
    expected_salary = models.BigIntegerField("Salary")
    salary_currency = models.CharField(
        max_length=3,
        choices=Currency.choices,
        default=Currency.CZK,
    )
    skills = models.TextField("List of skills")

    def __str__(self) -> str:
        return str(
            self.first_name + " " + self.last_name + " (email: " + self.email + ")"
        )


class Job(models.Model):
    """
    model for basic job info
    """

    job_title = models.CharField("Job Title", max_length=50, unique=True)
    job_description = models.TextField("Job Description")
    salary = models.BigIntegerField("Salary")
    salary_currency = models.CharField(
        max_length=3,
        choices=Currency.choices,
        default=Currency.CZK,
    )

    def __str__(self) -> str:
        return self.job_title


class Application(models.Model):
    """
    model with foreign key to job and candidate
    """

    applicant = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    status = models.CharField(
        max_length=2,
        choices=Status.choices,
        default=Status.AP,
    )

    class Meta:
        unique_together = [["applicant", "job"]]

    def __str__(self) -> str:
        return str(
            "APPLICANT: " + self.applicant.__str__() + " FOR JOB: " + self.job.__str__()
        )
