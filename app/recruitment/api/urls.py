from typing import Pattern
from django.contrib import admin
from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import ApplicantionViewSet, CandidateViewSet, JobViewSet, ApiRedirectView


router = DefaultRouter()
router.register(r"candidates", CandidateViewSet, basename="candidate")
router.register(r"jobs", JobViewSet, basename="job")
router.register(r"applications", ApplicantionViewSet, basename="application")

urlpatterns = router.urls
