from .views import CandidateViewSet, JobViewSet, ApplicantionViewSet
from django.urls import reverse
import json
from .serializers import (
    CandidateSerializer,
    JobSerializer,
    ApplicationSerializer,
    ApplicationFullInfoSerializer,
)
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import (
    APITestCase,
    force_authenticate,
    APIRequestFactory,
)
from .models import (
    Candidate,
    Currency,
    Job,
    Application,
    Status,
)


class CandidateViewTest(APITestCase):
    """
    Test for API GET /attributes
    """

    def setUp(self):
        candidate1 = Candidate.objects.create(
            first_name="Michal",
            last_name="Great",
            email="test@test.com",
            expected_salary=5000,
            salary_currency=Currency.CZK,
            skills="Python, Django",
        )

    def test_get_list_ok(self):
        view = CandidateViewSet.as_view({"get": "list"})
        url = reverse("candidate-list")
        request = APIRequestFactory().get(url, format="json")
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_get_detail_ok(self):
        candidate_object = Candidate.objects.get()
        view = CandidateViewSet.as_view({"get": "retrieve"})
        url = reverse("candidate-detail", args=(candidate_object.id,))
        request = APIRequestFactory().get(url, format="json")
        response = view(request, pk=candidate_object.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 7)
        self.assertEqual(response.data, CandidateSerializer(candidate_object).data)

    def test_put_ok(self):
        candidate_to_change = Candidate.objects.get()
        candidate_to_change.email = "test@zmena.com"
        view = CandidateViewSet.as_view({"put": "update"})
        url = reverse("candidate-detail", args=(candidate_to_change.id,))
        request = APIRequestFactory().put(
            url, CandidateSerializer(candidate_to_change).data, format="json"
        )
        response = view(request, pk=candidate_to_change.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        candidate_changed = Candidate.objects.get(pk=candidate_to_change.id)
        self.assertEqual(candidate_changed.email, "test@zmena.com")

    def test_post_ok(self):
        candidate2 = [
            {
                "first_name": "Jan",
                "last_name": "Small",
                "email": "test2@test.com",
                "expected_salary": 5000,
                "salary_currency": "CZK",
                "skills": "Python, Django",
            },
        ]
        view = CandidateViewSet.as_view({"post": "list"})
        url = reverse("candidate-list")
        request = APIRequestFactory().post(url, candidate2, format="json")
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Candidate.objects.count(), 1)

    def test_delete_ok(self):
        candidate_to_delete = Candidate.objects.get()
        candidates_count = Candidate.objects.count()
        view = CandidateViewSet.as_view({"delete": "destroy"})
        url = reverse("candidate-detail", args=(candidate_to_delete.id,))
        request = APIRequestFactory().delete(url, format="json")
        response = view(request, pk=candidate_to_delete.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(candidates_count - 1, Candidate.objects.count())


class JobViewTest(APITestCase):
    """
    Test for API GET /attributes
    """

    def setUp(self):
        job1 = Job.objects.create(
            job_title="Best job",
            job_description="really good job",
            salary=5000,
            salary_currency=Currency.CZK,
        )

    def test_get_list_ok(self):
        view = JobViewSet.as_view({"get": "list"})
        url = reverse("job-list")
        request = APIRequestFactory().get(url, format="json")
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_get_detail_ok(self):
        job_object = Job.objects.get()
        view = JobViewSet.as_view({"get": "retrieve"})
        url = reverse("job-detail", args=(job_object.id,))
        request = APIRequestFactory().get(url, format="json")
        response = view(request, pk=job_object.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 5)
        self.assertEqual(response.data, JobSerializer(job_object).data)

    def test_put_ok(self):
        job_to_change = Job.objects.get()
        job_to_change.job_description = "not that good job anymore"
        view = JobViewSet.as_view({"put": "update"})
        url = reverse("job-detail", args=(job_to_change.id,))
        request = APIRequestFactory().put(
            url, JobSerializer(job_to_change).data, format="json"
        )
        response = view(request, pk=job_to_change.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        job_changed = Job.objects.get(pk=job_to_change.id)
        self.assertEqual(job_changed.job_description, "not that good job anymore")

    def test_post_ok(self):
        job = [
            {
                "job_title": "Job",
                "job_description": "Job and job and job",
                "salary": 5000,
                "salary_currency": "CZK",
            },
        ]
        view = JobViewSet.as_view({"post": "list"})
        url = reverse("candidate-list")
        request = APIRequestFactory().post(url, job, format="json")
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Job.objects.count(), 1)

    def test_delete_ok(self):
        job_to_delete = Job.objects.get()
        jobs_count = Job.objects.count()
        view = JobViewSet.as_view({"delete": "destroy"})
        url = reverse("job-detail", args=(job_to_delete.id,))
        request = APIRequestFactory().delete(url, format="json")
        response = view(request, pk=job_to_delete.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(jobs_count - 1, Job.objects.count())


class ApplicationViewTest(APITestCase):
    """
    Test for API GET /attributes
    """

    def setUp(self):
        job1 = Job.objects.create(
            job_title="Best job",
            job_description="really good job",
            salary=5000,
            salary_currency=Currency.CZK,
        )
        candidate1 = Candidate.objects.create(
            first_name="Michal",
            last_name="Great",
            email="test@test.com",
            expected_salary=5000,
            salary_currency=Currency.CZK,
            skills="Python, Django",
        )
        application1 = Application.objects.create(
            applicant=candidate1, job=job1, status=Status.AP,
        )
        self.user = User.objects.create(
            username="admin", password="admin", is_superuser=True
        )

    def test_get_list_ok(self):
        view = ApplicantionViewSet.as_view({"get": "list"})
        url = reverse("application-list")
        request = APIRequestFactory().get(url, format="json")
        force_authenticate(request, user=self.user)
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_get_detail_ok(self):
        app_object = Application.objects.get()
        view = ApplicantionViewSet.as_view({"get": "retrieve"})
        url = reverse("application-detail", args=(app_object.id,))
        request = APIRequestFactory().get(url, format="json")
        force_authenticate(request, user=self.user)
        response = view(request, pk=app_object.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(
            json.dumps(response.data[0]),
            json.dumps(ApplicationFullInfoSerializer(app_object).data),
        )

    def test_put_ok(self):
        app_to_change = Application.objects.get()
        app_to_change.status = Status.IP
        view = ApplicantionViewSet.as_view({"put": "update"})
        url = reverse("application-detail", args=(app_to_change.id,))
        request = APIRequestFactory().put(
            url, ApplicationSerializer(app_to_change).data, format="json"
        )
        force_authenticate(request, user=self.user)
        response = view(request, pk=app_to_change.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        app_changed = Application.objects.get(pk=app_to_change.id)
        self.assertEqual(app_changed.status, Status.IP)

    def test_post_ok(self):
        app = [
            {"applicant": 1, "job": 1,},
        ]
        view = ApplicantionViewSet.as_view({"post": "list"})
        url = reverse("application-list")
        request = APIRequestFactory().post(url, app, format="json")
        force_authenticate(request, user=self.user)
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Application.objects.count(), 1)

    def test_delete_ok(self):
        app_to_delete = Application.objects.get()
        apps_count = Application.objects.count()
        view = ApplicantionViewSet.as_view({"delete": "destroy"})
        url = reverse("application-detail", args=(app_to_delete.id,))
        request = APIRequestFactory().delete(url, format="json")
        force_authenticate(request, user=self.user)
        response = view(request, pk=app_to_delete.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(apps_count - 1, Application.objects.count())

